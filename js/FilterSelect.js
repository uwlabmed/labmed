/** This file contains code that is used to restrict a SELECT list to a subset
 * that matches a user specified pattern.
 *
 * To use this, construct two arrays that contain the full set of Option text
 * and value properties. Next create a FilterSelect object passing the SELECT
 * element and the two arrays. Then, create a small text input element that the user
 * uses to specify the pattern. It looks like:
 *       <INPUT TYPE=TEXT NAME="selectFilterSelectText"
 *        SIZE=5
 *        ONCHANGE="performSelectFiltering(filterSelect, this.value)"
 *        ONKEYUP="return filterSelectHandler(event, 'filterSelect')"
 *        ONMOUSEUP="return filterSelectHandler(event, 'filterSelect')">
 *
 * There will be one of these for each SELECT that is filtered. Note that a
 * FilterSelect object needs to be created and stored in a variable that in
 * this example is named filterSelect. The FilterSelect object allows one to
 * have more than one filtered SELECT on a page. When using more than one
 * filtered SELECT, use a different variable to hold the object and specify the
 * other variable name in the ON handlers listed above. Note that the variable
 * name is quoted in the ON*UP handlers so an eval can be performed to get the
 * FilterSelect object. Be sure change the NAME property of the TEXT element
 * too. The object will contain the other SELECT object and the possibly
 * different optionTexts and optionValues arrays.
 */

/** Define a variable that contains help text for the FilterSelect feature.
 * This is intended to be appended to the end of another help string. */
var filterSelectHelp = "\n\nThe narrow text box to the left of this field can be used to reduce the number of items displayed in the menu. This is done by specifying a pattern that is used to match with each menu item. Only the menu items that match the pattern will be displayed.\n\nThe simplest pattern is ordinary text. Any menu item that contains that text (case matters) will be displayed.\n\nIf you want to display menu items that start with something, put a caret (^, Shift-6) character at the beginning of the pattern. For example, the pattern ^contact will only display those menu items that start with 'contact'.\n\nSimilarly, if you want to display menu items that end with something, put a dollar sign ($, Shift-4) at the end of the pattern. For example, the pattern assigned_to$ will only display those menu items that end with 'assigned_to'.\n\nThe following is a brief listing of the more advanced features. A feature marked with a * is one that you may find useful.\n    [...], [^...] positive and negative character class, respectively.\n    . matches any single character.\n    \\w matches a 'word' character. Same as [a-zA-Z0-9_].\n    \\W matches a non-'word' character. Same as [^a-zA-Z0-9_].\n    \\s matches whitespace.\n    \\S matches anything that is not whitespace.\n    \\d matches a digit character. Same as [0-9].\n    \\D matches a non-digit character. Same as [^0-9].\n    \\b matches a word boundary. This is the possition between a \\w character and an \\W character, or between a \\w character and the beginning or end of a string. \n    \\B matches a position that is not a word boundary.\n    (?=p) positive look-ahead for pattern 'p'.\n    (?!p) negative look-ahead for pattern 'p'.\n\nThe following apply to the preceeding pattern.\n    {n,m} match the previous pattern at least n times but no more than m times. If m is omitted, as in {n,} then m is assumed to be infinity.\n    {n} match the previous pattern exactly n times.\n    ? match the previous pattern 0 or 1 times. That is, the previous item is optional. Same as {0,1}.\n*   + match the previous pattern 1 or more times. Same as {1,}.\n*   * match the previous pattern 0 or more times. Same as {0,}.\n\nGrouping and alternation\n*   | match either the pattern to the left or the pattern to the right.\n    (...) group patterns into a single unit that can be used with *, +, ?, etc. This also remembers the characters that match this group for later references.\n    (?:...) same as (...) except it does not remember matching characters.\n    \\n match the characters that were matched when group number n was matched. Group numbers are assigned by counting left parentheses from left to right.\n       Groups formed with (?: are not numbered.\n\nExamples:\n\ncontact|UWMC\nThis will display only those menu items that contain either 'contact' or 'UWMC'.\n\n[jkz]\nThis will display only those menu items that contain one of the letters 'j', 'k' or 'z'.\n\n(compl).*\\1\nThis will only display those menu items that contain two sets of 'compl' characters. It does this by first looking for the first occurrence of the 'compl' characters and remembering that. After the 'compl' characters can come 0 or more single characters. After that it looks for a match with the first remembered match, which is the second occurrence of the 'compl' characters.";

//-----------------------------------------------------------------------------
/**
 * Constructor function for a FilterSelect object.
 *
 * @param  selectElement  the HTML SELECT object that contains the options to be
 * filtered.
 *
 * @param  optionTexts  an array of values for the SELECT Option text property.
 *
 * @param  optionValues  an array of values for the SELECT Option value
 * property.
 */
//-----------------------------------------------------------------------------
function FilterSelect(selectElement, optionTexts, optionValues) {
    // Timeout ID for filter changes. This is null if no timeout is active.
    this.timeoutID = null;

    // This specifies the SELECT list element that contains the attributes.
    this.selectElement = selectElement;

    // These hold the full set of Option text and value properties from which
    // the user will filter.
    this.optionTexts = optionTexts;
    this.optionValues = optionValues;

    // Indicates how upper/lower case is to be handled when filtering.
    this.ignoreCase = false;

    // This is a function to call after the user has made a change to the
    // filter pattern. The parameter passed to this function is the SELECT
    // list.
    this.changeHandler = null;

    // Define the methods.
    this.toString         = FilterSelect_toString;
    this.setIgnoreCase    = FilterSelect_setIgnoreCase;
    this.getIgnoreCase    = FilterSelect_getIgnoreCase;
    this.getChangeHandler = FilterSelect_getChangeHandler;
    this.setChangeHandler = FilterSelect_setChangeHandler;
    this.getPattern       = FilterSelect_getPattern;
    this.setOptionArrays  = FilterSelect_setOptionArrays;
}


//-----------------------------------------------------------------------------
/**
 * Used to display the instance variables of a FilterSelect object.
 *
 * @return
 *   String - a nicely formatted FilterSelect object.
 */
//-----------------------------------------------------------------------------
function FilterSelect_toString() {
    var funInfo = (this.changeHandler == null
                ? "(null)"
                : this.changeHandler.toString());
    if (funInfo.length > 40) {
        var splits = funInfo.split("\n");
        funInfo = trim(splits[0]);
    }
    return "FilterSelect[" +
                "timeoutID=" + this.timeoutID +
                ", ignoreCase=" + this.ignoreCase +
                ", selectElement.name=" + this.selectElement.name +
                ", changeHandler=" + funInfo +
                ", optionTexts.length=" + this.optionTexts.length +
                ", optionValues.length=" + this.optionValues.length +
                "]";
}


//-----------------------------------------------------------------------------
/**
 * Indicate that case is to be ignore when filtering.
 */
//-----------------------------------------------------------------------------
function FilterSelect_setIgnoreCase() {
    this.ignoreCase = true;
}


//-----------------------------------------------------------------------------
/**
 * Get the setting of the ignoreCase instance variable.
 *
 * @return
 *   boolean - true if case is to be ignored, false if upper/lower case matters.
 */
//-----------------------------------------------------------------------------
function FilterSelect_getIgnoreCase() {
    return this.ignoreCase;
}


//-----------------------------------------------------------------------------
/**
 * Returns the current setting for the changeHandler.
 *
 * @return
 *   Function - the changeHandler function.
 */
//-----------------------------------------------------------------------------
function FilterSelect_getChangeHandler() {
    return this.changeHandler;
}


//-----------------------------------------------------------------------------
/**
 * Set the value for a function to call when the user changes the Filter Select
 * pattern.
 *
 * @param  changeHandler  the new changeHandler function.
 */
//-----------------------------------------------------------------------------
function FilterSelect_setChangeHandler(changeHandler) {
    this.changeHandler = changeHandler;
}


//-----------------------------------------------------------------------------
/**
 * Determine a regular expression pattern by using a pattern string.
 *
 * @param  patternString  a string representation of the filter pattern.
 *
 * @return
 *   RegExp - if the regular expression is valid, the object is returned,
 * otherwise null is returned.
 */
//-----------------------------------------------------------------------------
function FilterSelect_getPattern(patternString) {
    var pattern = null;

    try {
        pattern = (this.getIgnoreCase()
                    ? new RegExp(patternString, "i")
                    : new RegExp(patternString));
    }
    catch (e) {
        // Got a regular expression error.
    }
    return pattern;
}


//-----------------------------------------------------------------------------
/**
 * When the option arrays change, call this to allow FilterSelect to work with
 * the new values.
 *
 * @param  optionTexts  the new Option Texts array.
 *
 * @param  optionValues  the new Option Values array.
 */
//-----------------------------------------------------------------------------
function FilterSelect_setOptionArrays(optionTexts, optionValues) {
    this.optionTexts  = optionTexts;
    this.optionValues = optionValues;
}


//-----------------------------------------------------------------------------
/**
 * The user has or may have made a change to the select filter pattern TEXT
 * element. At this point the change is not reflected in that TEXT element, so
 * we have to wait a while for the change to take effect.
 *
 * @param  e  event object. Used to get the element that generated the event.
 *
 * @param  filterSelectVar  a variable name (a string) that holds a
 * FilterSelect object that is used to hold various values that are needed to
 * support this feature.
 */
//-----------------------------------------------------------------------------
function filterSelectHandler(e, filterSelectVar) {
    var filterSelect = eval("window." + filterSelectVar);
    if (filterSelect.timeoutID != null) {
        clearTimeout(filterSelect.timeoutID);
        filterSelect.timeoutID = null;
    }
    var element = e.target ? e.target : e.srcElement;
    var formName = element.form.name;
    if (typeof(formName) != "string") {
        // Default to missing if we can't determine the form name.
        formName = "missing";
    }
    filterSelect.timeoutID = setTimeout(
                "performSelectFiltering(window." +
                filterSelectVar + "," +
                " document." + formName + "." +
                element.name + ".value)", 100);
    return true;
}


//-----------------------------------------------------------------------------
/**
 * The user has or may have made a change to the select filter pattern. Update
 * the SELECT list to reflect the filter change. Care is taken to keep the
 * currently selected item selected, and to not change the select list when the
 * filter selects the same options.
 *
 * @param  filterSelect  a  FilterSelect object that is used to hold various
 * values that are needed to support this feature.
 *
 * @param  patternString  the new pattern to match against the optionTexts
 * array.
 */
//-----------------------------------------------------------------------------
function performSelectFiltering(filterSelect, patternString) {
    var i;
    var text;
    var newTexts = new Array();
    var newValues = new Array();
    var changed = false;
    var selectedText;
    var selected;
    var pattern;
    var sel    = filterSelect.selectElement;
    var texts  = filterSelect.optionTexts;
    var values = filterSelect.optionValues;

    filterSelect.timeoutID = null;

    pattern = filterSelect.getPattern(patternString);
    if (pattern == null)
        // Got a regular expression error. Don't do anything more.
        return;

    // Collect the text values that match this pattern. Note that the pattern
    // is created outside of this loop so it is only created once.
    for(i=0; i<texts.length; i++) {
        text = texts[i];
        if (text.match(pattern)) {
            newTexts.push(text);
            newValues.push(values[i]);
        }
    }

    changed = sel.options.length != newTexts.length;
    if (!changed) {
        // The length is the same. Now look at what matched to see if that is
        // the same.
        for(i=0; i<newTexts.length; i++) {
            if (sel.options[i].text != newTexts[i]) {
                changed = true;
                break;
            }
        }
    }
    if (changed) {
        // Only update the SELECT if the pattern caused something to change.
        // Also, be sure to maintain the selected column so that the user isn't
        // confused by an apparently random selection.
        selectedText = sel.selectedIndex >= 0
                    ? sel.options[sel.selectedIndex].text
                    : "";
        sel.options.length = 0;
        for(i=0; i<newTexts.length; i++) {
            selected = equalHTMLtoJS(selectedText, newTexts[i]);
            sel.options[i] = new Option(newTexts[i], newValues[i],
                        selected, selected);
        }
        if (filterSelect.getChangeHandler() != null) {
            eval(filterSelect.getChangeHandler() + "(filterSelect)");
        }

        // Only run the onchange handler once.
        var onchangeHandlerRan = false;
        if (sel.options.length == 1) {
            // Only one option showing. Select it.
            sel.options[0].selected = true;
            runOnchangeHandler(sel);
            onchangeHandlerRan = true;
        }
        if (sel.size == 1) {
            // Menus with only one element should execute the SELECT's onchange handler.
            if (!onchangeHandlerRan)
                runOnchangeHandler(sel);
        }
    }
}


//-----------------------------------------------------------------------------
/**
 * Run the element's onchange handler, if it is defined.
 *
 * @param  element  the element whose onchange handler is to be run.
 */
//-----------------------------------------------------------------------------
function runOnchangeHandler(element) {
    if (typeof(element.onchange) == "function") {
        try {
            element.onchange();
        }
        catch (e) {
            // Got an error in the onchange handler. Ignore it.
        }
    }
}


//-----------------------------------------------------------------------------
/**
 * Perform an equal comparison between an HTML string and a JavaScript string.
 * An HTML string may contain &nbsp; characters which should compare equal to
 * corresponding JavaScript " " characters.
 *
 * @param  html  the HTML string.
 *
 * @param  js  the JavaScript string.
 *
 * @return
 *   boolean - true if the two strings are equal, false if not.
 */
//-----------------------------------------------------------------------------
function equalHTMLtoJS(html, js) {
    var htmls = html.split('');
    var jss = js.split('');
    var ret = false;
    var i;

    if (htmls.length == jss.length) {
        ret = true;
        for(i=0; i<htmls.length; i++) {
            if ((htmls[i] == "\xA0") && (jss[i] == " "))
                continue;
            if ((htmls[i] == "\xB4") && (jss[i] == "'"))
                continue;
            if (htmls[i] != jss[i]) {
                ret = false;
                break;
            }
        }
    }
    return ret;   
}
