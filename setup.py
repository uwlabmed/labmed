from os import path
import subprocess

from distutils.core import setup
from setuptools import find_packages

# Provide the abbreviated git sha and the svn revision number as
# package data. These commands are run whenever setup.py is
# invoked. Use `python setup.py -h` to refresh files in labmed/data
# without building or installing.
subprocess.call('git log --pretty=format:%h -n 1 > labmed/data/sha', shell=True)
subprocess.call("svn info 2> /dev/null| grep Revision | cut -d' ' -f2 > labmed/data/rev", shell=True)
from labmed import __version__

params = {'author': 'Tom Ekberg',
          'author_email': 'tekberg@uw.edu',
          'description': 'LabMed common code',
          'name': 'labmed',
          'package_dir': {'labmed': 'labmed'},
          'packages': find_packages(),
          'scripts': [],
          'url': 'https://uwmc-labmed.beanstalkapp.com/developers/browse/labmed',
          'version': __version__,
          'package_data': {'labmed': [path.join('data',f) for f in ['sha','rev']]},
          'zip_safe': False,            # Force directory instead of egg file
          }

setup(**params)
