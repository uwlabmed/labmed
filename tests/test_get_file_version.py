"""
Test factory function for get_file_version.
"""

import sys
import os
if 'PYTHONPATH' in os.environ:
    # Put at the top of the path list.
    # This gets the labmed code from here, instead of the installed version.
    sys.path = [os.environ['PYTHONPATH']] + sys.path

from os import path
import unittest
from datetime import datetime
import re
import traceback

import labmed.get_file_version


class TestGetModel(unittest.TestCase):
    def test_version(self):
        expected_ret = ('2014-07-30 12:00', '2014-07-31 09:33:22', True)
        expected_line = 'Line 2\n'
        with open('data/test-version.txt') as in_file:
            got = labmed.get_file_version.get_version(in_file)
            line = next(in_file)

        self.assertEqual(got, expected_ret)
        self.assertEqual(line, expected_line)

    def test_no_version(self):
        expected_ret = (None, None, False)
        expected_line = 'Line 1\n'
        with open('data/test-no-version.txt') as in_file:
            got = labmed.get_file_version.get_version(in_file)
            line = next(in_file)

        self.assertEqual(got, expected_ret)
        self.assertEqual(line, expected_line)

        
if __name__ == '__main__':
    unittest.main()
