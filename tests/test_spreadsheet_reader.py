"""
Test factory function for model classes
"""

import sys
import os
if 'PYTHONPATH' in os.environ:
    # Put at the top of the path list.
    # This gets the labmed code from here, instead of the installed version.
    sys.path = [os.environ['PYTHONPATH']] + sys.path

from os import path
import unittest
from datetime import datetime
import re
import traceback

import labmed.spreadsheet_reader

sep_data = [
    ['6', '903', '1', '1'],
    ['7', '554', '2', '24'],
    ['88', '577', '3', '624'],
    ['99', '238', '4', '15402'],
    ['100', '39', '5', '389553'],
    ]

big_header_data = [
    ['95', '1', '%AAN', 'H', 'T', 'AP ACCESSION NUMBER', 'AP', '', '', 'N', 'N', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'No reference range data'],
    ['95', '1', '%AAN', 'U', 'T', 'AP ACCESSION NUMBER', 'AP', '', '', 'N', 'N', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'No reference range data'],
    ]


class RowHandler(labmed.spreadsheet_reader.BaseRowHandler):
    def __init__(self, column_names, test_instance, test_data, column_name, column_number):
        labmed.spreadsheet_reader.BaseRowHandler.__init__(self, None, None, column_names, False)
        self.test_instance = test_instance
        self.count = 0
        self.test_data = test_data
        self.column_name = column_name
        self.column_number = column_number

    def handle(self, row):
        expected_row = self.test_data[self.count]
        self.test_instance.assertEqual(row, expected_row)
        self.test_instance.assertEqual(row[self.column_map[self.column_name]], expected_row[self.column_number])
        self.count += 1
        

class TestGetModel(unittest.TestCase):
    def test_bar(self):
        act = labmed.spreadsheet_reader.Action('data/test-bar.csv', RowHandler, row_handler_args={'test_instance':self, 'test_data': sep_data, 'column_name': 'd', 'column_number': 3})
        self.assertEqual(act.column_names, ['a','b','c','d'])
        act.loop()
        self.assertEqual(act.count, 5)

    def test_comma(self):
        act = labmed.spreadsheet_reader.Action('data/test-comma.csv', RowHandler, row_handler_args={'test_instance':self, 'test_data': sep_data, 'column_name': 'd', 'column_number': 3})
        self.assertEqual(act.column_names, ['a','b','c','d'])
        act.loop()
        self.assertEqual(act.count, 5)

    def test_semi(self):
        act = labmed.spreadsheet_reader.Action('data/test-semi.csv', RowHandler, row_handler_args={'test_instance':self, 'test_data': sep_data, 'column_name': 'd', 'column_number': 3})
        self.assertEqual(act.column_names, ['a','b','c','d'])
        act.loop()
        self.assertEqual(act.count, 5)

    def test_tab(self):
        act = labmed.spreadsheet_reader.Action('data/test-tab.csv', RowHandler, row_handler_args={'test_instance':self, 'test_data': sep_data, 'column_name': 'd', 'column_number': 3})
        self.assertEqual(act.column_names, ['a','b','c','d'])
        act.loop()
        self.assertEqual(act.count, 5)

    def test_big_header(self):
        act = labmed.spreadsheet_reader.Action('data/test-big_header.csv.gz', RowHandler, row_handler_args={'test_instance':self, 'test_data': big_header_data, 'column_name': 'mnemonic', 'column_number': 2})
        self.maxDiff = None
        self.assertEqual(act.column_names, ['rec_len', 'number', 'mnemonic', 'hosp', 'type', 'lab_name', 'dept', 'dept_name', 'unit', 'o_flg', 'sp_flg', 'b_code_number_cpt', 'components', 'name1', 'name2', 'sp_type', 'bcrr_1', 'bcrr_2', 'bcrr_3', 'collect_1', 'collect_2', 'amount', 'minimum', 'sp_hand_1', 'sp_hand_2', 'sp_hand_3', 'sp_hand_4', 'sp_hand_5', 'sp_hand_6', 'done_uwmc', 'done_hmc', 'done_oth_1', 'done_oth_2', 'done_oth_3', 'done_oth_4', 'freq_1', 'freq_2', 'avl_stat', 'x_ref', 'updated', 'sec_flg', 'cautions', 'rr_eff_date', 'rr_units', 'f_age_rng01', 'f_ref_rng01', 'f_age_rng02', 'f_ref_rng02', 'f_age_rng03', 'f_ref_rng03', 'f_age_rng04', 'f_ref_rng04', 'f_age_rng05', 'f_ref_rng05', 'f_age_rng06', 'f_ref_rng06', 'f_age_rng07', 'f_ref_rng07', 'f_age_rng08', 'f_ref_rng08', 'f_age_rng09', 'f_ref_rng09', 'f_age_rng10', 'f_ref_rng10', 'f_age_rng11', 'f_ref_rng11', 'f_age_rng12', 'f_ref_rng12', 'f_age_rng13', 'f_ref_rng13', 'f_age_rng14', 'f_ref_rng14', 'f_age_rng15', 'f_ref_rng15', 'f_age_rng16', 'f_ref_rng16', 'f_age_rng17', 'f_ref_rng17', 'f_age_rng18', 'f_ref_rng18', 'f_age_rng19', 'f_ref_rng19', 'f_age_rng20', 'f_ref_rng20', 'f_age_rng21', 'f_ref_rng21', 'f_age_rng22', 'f_ref_rng22', 'f_age_rng23', 'f_ref_rng23', 'f_age_rng24', 'f_ref_rng24', 'f_age_rng25', 'f_ref_rng25', 'm_age_rng01', 'm_ref_rng01', 'm_age_rng02', 'm_ref_rng02', 'm_age_rng03', 'm_ref_rng03', 'm_age_rng04', 'm_ref_rng04', 'm_age_rng05', 'm_ref_rng05', 'm_age_rng06', 'm_ref_rng06', 'm_age_rng07', 'm_ref_rng07', 'm_age_rng08', 'm_ref_rng08', 'm_age_rng09', 'm_ref_rng09', 'm_age_rng10', 'm_ref_rng10', 'm_age_rng11', 'm_ref_rng11', 'm_age_rng12', 'm_ref_rng12', 'm_age_rng13', 'm_ref_rng13', 'm_age_rng14', 'm_ref_rng14', 'm_age_rng15', 'm_ref_rng15', 'm_age_rng16', 'm_ref_rng16', 'm_age_rng17', 'm_ref_rng17', 'm_age_rng18', 'm_ref_rng18', 'm_age_rng19', 'm_ref_rng19', 'm_age_rng20', 'm_ref_rng20', 'm_age_rng21', 'm_ref_rng21', 'm_age_rng22', 'm_ref_rng22', 'm_age_rng23', 'm_ref_rng23', 'm_age_rng24', 'm_ref_rng24', 'm_age_rng25', 'm_ref_rng25'])
        act.loop()
        self.assertEqual(act.count, 2)


        
if __name__ == '__main__':
    unittest.main()
