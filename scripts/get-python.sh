#!/bin/sh
#
# Return the full path for a good version of python.
#
# Some machines have the good version in /usr/bin and some have it in
# /usr/local/bin. Look in those directories and stop when a good version is
# found. Outputs the full path to standard output. If no version is found, an
# empty string is outputted.
#
#------------------------------------------------------------------------------

pyprog=""
max_minor=1
pydir=""
for dir in `printf "/usr/bin\n/usr/local/bin"` ; do
    if [ -x "$dir/python" ]; then
        pyver=`$dir/python -V 2>&1 | cut '-d ' -f2`
        major=`echo $pyver | cut -d. -f1`
        minor=`echo $pyver | cut -d. -f2`
        if [ $max_minor -lt $minor ]; then
            max_minor=$minor
            pydir="$dir/"
        fi
    fi
done

echo "${pydir}python"
