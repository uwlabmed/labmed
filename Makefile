#  @(#) $Id:  $  

### *********************************************************************
###
###   Copyright (c) 2011 University of Washington Laboratory Medicine
###   All Rights Reserved
###
###   The information contained herein is confidential to and the
###   property of University of Washington Laboratory Medicine and is
###   not to be disclosed to any third party without prior express
###   written permission of University of Washington Laboratory Medicine.
###   University of Washington Laboratory Medicine, as the
###   author and owner under 17 U.S.C. Sec. 201(b) of this work made
###   for hire, claims copyright in this material as an unpublished
###   work under 17 U.S.C. Sec.s 102 and 104(a)
###
### ******************************************************************* 


RSTFLAGS=--generator --date --toc-top-backlinks
PYTHON=`scripts/get-python.sh`

APP = labmed
# Specify what type of database is to be used: mysql or postgresql.
DB_TYPE=postgresql

pycheck:
	@echo PYTHON = ${PYTHON}; \
	${PYTHON} -V

# Build the labmed package and install it somewhere.
install:
	${PYTHON} setup.py install ; \
	${PYTHON} -V

doc: README.html

README.html: README.txt
	rst2html ${RSTFLAGS} README.txt README.html

test:
	@${PYTHON} labmed/util/date_time_parser.py
	@${PYTHON} labmed/util/pluralize.py
	./runtest get_file_version spreadsheet_reader

#------------------------------------------------------------------------------

help:
	@printf "\
Targets:\n\
  cleansrc - remove emacs backup files.\n\
  doc - rebuild the HTML file from README.txt.\n\
  install - install for Python.\n\
  pycheck - check the version of python.\n\
  tags - make TAGS file for emacs.\n\
  test - execute test cases.\n\
"

cleansrc:
	@find . -name '*~' -exec rm -v {} + ; \
	find . -name '*.bak' -exec rm -vf {} +

tags:
	@cd $(APP) ; \
	/usr/local/bin/eptags -R
