.. -*- mode: rst -*-

============
Introduction
============

This directory contains things that shouldn't be placed into
individual projects. Mostly these are python utility modules.

========
Contents
========

labmed.filter_select
====================

Python program that allows one to filter an HTML SELECT element when the list
is long. The user enters characters into a small HTML TEXT box next to the
SELECT element to filter on those OPTION text values that match the
characters. Javascript regular expressions are used to allow for a more general
filter. Look at the documentation string to see how to use this module.  The
js/FilterSelect.js file provides the runtime support for this feature.

get_file_version
================

This module contains the get_version function that tries to read a version
number from a file. This is useful when the format of a file changes.

labmed.spreadsheet_reader
=========================

This module puts all of the features one would want in a spreadsheet reader in
one place. It reads CSV, xls and xlsx files (the CSV files can be
compressed). It automatically picks the right reader based on the file
extension.  Provides a DefaultRowHandler that performs processing on a single
row. Normally one would write a new RowHandler class extending BaseRowHandler.

Using this dropped the code needed to do the same thing from 403 lines to 185.

labmed.util.date_time_parser
============================

General purpose date and/or time parser that support multiple formats.

labmed.util.pluralize
=====================

Takes an American English word and converts to its plural form. Handles many
special cases. Its two primary functions take a number and a string in singular
form. If the number is one the string is returned. Otherwise the plural form is
returned. One function returns a string containing the number and the plural or
singular form. The other function just returns the plural or singular form.

labmed.util.unicode
===================

This in used by code that doesn't handle general unicode strings gracefully. It
converts any special characters into their HTML character entity equivalent.

labmed.util.boolean
===================

This contains the to_bool function that converts a value to boolean. It handles
strings like 'TRue', 't', 'yes', 'y', '1', for True, and 'False', 'f', 'no',
'0' '' for False. Other values are passed to bool for interpretation.

==========
Installing
==========

All building uses the make command. If you are installing the package to the
normal production python directory (/usr/lib/python2.x/site-packages)
you will need to be root, and specify no special args::

  sudo make install

If the root python is not the version you want, something like this
will work too::

  sudo /usr/local/bin/python setup.py install

=======
Testing
=======

To run through the unit tests execute do this::

  make test

All should indicate that they passed, others should indicate 'OK'.
