from os import path

for fname in ['sha', 'rev']:
    try:
        with open(path.join(path.dirname(__file__), 'data', fname)) as f:
            ver = f.read().strip()
    except Exception:
        ver = ''

    if ver:
        break
    
__version__ = "1.0" + ('.' + ver if ver else '') 
__version_info__ = tuple([int(num) if num.isdigit() else num for num in __version__.split('.')])

