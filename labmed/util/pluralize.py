#  @(#) $Id:  $  

### *********************************************************************
###
###   Copyright (c) 2010 University of Washington Medical Center
###   All Rights Reserved
###
###   The information contained herein is confidential to and the
###   property of University of Washington Medical Center and is not to be disclosed
###   to any third party without prior express written permission
###   of University of Washington Medical Center.
###   University of Washington Medical Center, as the
###   author and owner under 17 U.S.C. Sec. 201(b) of this work made
###   for hire, claims copyright in this material as an unpublished
###   work under 17 U.S.C. Sec.s 102 and 104(a)
###
### *******************************************************************


"""
Handles American English plualizing words. Does a lot more than tack
an 's' to the end of the word.

Run from the command line to exercise test cases.
"""
 
 
def loader():
    import re

    # (pattern, search, replace) regex english plural rules tuple.
    # First look for pattern. If found substitute search with replace.
    rule_tuple = (
        ('[ml]ouse$', '([ml])ouse$', '\\1ice'), 
        ('child$', 'child$', 'children'), 
        ('booth$', 'booth$', 'booths'), 
        ('foot$', 'foot$', 'feet'), 
        ('ooth$', 'ooth$', 'eeth'), 
        ('l[eo]af$', 'l([eo])af$', 'l\\1aves'), 
        ('oof$', 'oof$', 'ooves'),
        ('sis$', 'sis$', 'ses'), 
        ('man$', 'man$', 'men'), 
        ('ife$', 'ife$', 'ives'), 
        ('eau$', 'eau$', 'eaux'),
        ('cho$', 'cho$', 'choes'),
        ('goose$', 'goose$', 'geese'),
        ('[a-z]go$', '([a-z])go$', '\\1goes'),
        ('person$', 'person$', 'people'),
        ('lf$', 'lf$', 'lves'),
        ('racks|deer$', '(racks|deer)$', '\\1'), # singular == plural
        ('leus$', 'leus$', 'lei'),
        ('menon$', 'menon$', 'mena'),
        ('[bcgt]us$', '([bcgt])us$', '\\1i'),
        ('dix$', 'dix$', 'dices'),
        ('rion$', 'rion$', 'ria'),
        ('[osxz]$', '$', 'es'), 
        ('[^aeioudgkprt]h$', '$', 'es'), 
        ('(qu|[^aeiou])y$', 'y$', 'ies'), 
        ('$', '$', 's')
        )
 
    def regex_rules(rules=rule_tuple):
        for line in rules:
            pattern, search, replace = line
            yield lambda word: re.search(pattern, word) and re.sub(search, replace, word)
 
    global plural
    def plural(singular):
        """
        plural(singular) -> plural

        Params:
          singular - singular form of a noun.

        Returns:
          plural form of singular.
        """
        for rule in regex_rules():
            result = rule(singular)
            if result: 
                return result
 
    global just_pluralize
    def just_pluralize(count, singular):
        """
        just_pluralize(count, singular)

        Params:
          count - a count of the number of singular (e.g. number of hams)
          singular - singular form of the noun (e.g. ham)

        Returns:
          If count is 1, then returns singular.
          If count isn't 1 then returns plural(singular).

        Examples:
          just_pluralize(1, "ham") -> "ham"
          just_pluralize(2, "ham") -> "hams"
          just_pluralize(0, "ham") -> "hams"
        """
        return singular if count == 1 else plural(singular)


    global pluralize
    def pluralize(count, singular):
        """
        pluralize(count, singular)

        Params:
          count - a count of the number of singular (e.g. number of hams)
          singular - singular form of the noun (e.g. ham)

        Returns:
          If count is 1, then returns a string of the count and singular.
          If count isn't 1 then returns a string of the count and plural(singular).

        Examples:
          "1 ham"
          "2 hams"
          "0 hams"
        """
        return '%d %s' % (count, just_pluralize(count, singular))


loader()
del loader

if __name__ == '__main__':
    plural_test_cases = [
        ('appendix', 'appendices'),
        ('barracks', 'barracks'),
        ('cactus', 'cacti'),
        ('child', 'children'),
        ('criterion', 'criteria'),
        ('deer', 'deer'),
        ('echo', 'echoes'),
        ('elf', 'elves'),
        ('embargo', 'embargoes'),
        ('focus', 'foci'),
        ('fungus', 'fungi'),
        ('goose', 'geese'),
        ('hero', 'heroes'),
        ('hoof', 'hooves'),
        ('index', 'indexes'),
        ('knife', 'knives'),
        ('lady', 'ladies'),
        ('leaf', 'leaves'),
        ('life', 'lives'),
        ('man', 'men'),
        ('mouse', 'mice'),
        ('nucleus', 'nuclei'),
        ('person', 'people'),
        ('phenomenon', 'phenomena'),
        ('potato', 'potatoes'),
        ('self', 'selves'),
        ('syllabus', 'syllabi'),
        ('tomato', 'tomatoes'),
        ('torpedo', 'torpedoes'),
        ('veto', 'vetoes'),
        ('wife', 'wives'),
        ('woman', 'women'),
        ]
    n_tests = 0
    n_passed = 0
    
    for (test, expected) in plural_test_cases:
        got = plural(test)
        n_tests += 1
        if expected == got:
            n_passed += 1
        else:
            print 'Test %d failed. For %s expected "%s", got "%s"' % (n_tests, test, expected, got)

    expected = 'hams'
    count = 0
    test = 'ham'
    got = just_pluralize(count, test)
    n_tests += 1
    if expected == got:
        n_passed += 1
    else:
        print 'Test %d failed. For just_pluralize(%d,"%s") expected "%s", got "%s"' % (n_tests, count, test, expected, got)

    expected = 'ham'
    count = 1
    test = 'ham'
    got = just_pluralize(count, test)
    n_tests += 1
    if expected == got:
        n_passed += 1
    else:
        print 'Test %d failed. For just_pluralize(%d,"%s") expected "%s", got "%s"' % (n_tests, count, test, expected, got)

    expected = 'hams'
    count = 2
    test = 'ham'
    got = just_pluralize(count, test)
    n_tests += 1
    if expected == got:
        n_passed += 1
    else:
        print 'Test %d failed. For just_pluralize(%d,"%s") expected "%s", got "%s"' % (n_tests, count, test, expected, got)

    expected = '0 hams'
    count = 0
    test = 'ham'
    got = pluralize(count, test)
    n_tests += 1
    if expected == got:
        n_passed += 1
    else:
        print 'Test %d failed. For pluralize(%d,"%s") expected "%s", got "%s"' % (n_tests, count, test, expected, got)

    expected = '1 ham'
    count = 1
    test = 'ham'
    got = pluralize(count, test)
    n_tests += 1
    if expected == got:
        n_passed += 1
    else:
        print 'Test %d failed. For pluralize(%d,"%s") expected "%s", got "%s"' % (n_tests, count, test, expected, got)

    expected = '2 hams'
    count = 2
    test = 'ham'
    got = pluralize(count, test)
    n_tests += 1
    if expected == got:
        n_passed += 1
    else:
        print 'Test %d failed. For pluralize(%d,"%s") expected "%s", got "%s"' % (n_tests, count, test, expected, got)

    print 'Out of %s, %d passed.' % (pluralize(n_tests, 'pluralize test'), n_passed)
