#  @(#) $Id:  $  

### *********************************************************************
###
###   Copyright (c) 2012 University of Washington Laboratory Medicine
###   All Rights Reserved
###
###   The information contained herein is confidential to and the
###   property of University of Washington Laboratory Medicine and is
###   not to be disclosed to any third party without prior express
###   written permission of University of Washington Laboratory Medicine.
###   University of Washington Laboratory Medicine, as the
###   author and owner under 17 U.S.C. Sec. 201(b) of this work made
###   for hire, claims copyright in this material as an unpublished 
###   work under 17 U.S.C. Sec.s 102 and 104(a)   
###
### *********************************************************************


"""
Defines a function that converts a value to boolean.

Handles many string values and is case insensitive.
"""

def to_bool(value):
        """
        Converts 'something' to boolean. Raises exception if it gets a string it doesn't handle.
        Case is ignored for strings. These string values are handled:
          True: 'True', "1", "TRue", "yes", "y", "t"
          False: "", "0", "faLse", "no", "n", "f"
        Non-string values are passed to bool.
        """
        if type(value) == type(''):
            if value.lower() in ("yes", "y", "true",  "t", "1"):
                return True
            if value.lower() in ("no",  "n", "false", "f", "0", ""):
                return False
            raise Exception('Invalid value for boolean conversion: ' + value)
        return bool(value)


if __name__ == '__main__':
    test_cases = [
        ('true', True),
        ('t', True),
        ('yes', True),
        ('y', True),
        ('1', True),
        ('false', False),
        ('f', False),
        ('no', False),
        ('n', False),
        ('0', False),
        ('', False),
        (1, True),
        (0, False),
        (1.0, True),
        (0.0, False),
        ([], False),
        ({}, False),
        ((), False),
        ([1], True),
        ({1:2}, True),
        ((1,), True),
        (None, False),
        (object(), True),
        ]
    n_tests = 0
    n_passed = 0
    for (test, expected) in test_cases:
        got = to_bool(test)
        n_tests += 1
        if expected == got:
            n_passed += 1
        else:
            print 'Test %d failed. For %s expected "%s", got "%s"' % (n_tests, test, expected, got)
    print 'Out of %d to_bool tests, %d passed.' % (n_tests, n_passed)
