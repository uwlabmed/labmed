#  @(#) $Id:  $  

### *********************************************************************
###
###   Copyright (c) 2011 University of Washington Laboratory Medicine
###   All Rights Reserved
###
###   The information contained herein is confidential to and the
###   property of University of Washington Laboratory Medicine and is
###   not to be disclosed to any third party without prior express
###   written permission of University of Washington Laboratory Medicine.
###   University of Washington Laboratory Medicine, as the
###   author and owner under 17 U.S.C. Sec. 201(b) of this work made
###   for hire, claims copyright in this material as an unpublished 
###   work under 17 U.S.C. Sec.s 102 and 104(a)   
###
### *********************************************************************


"""
Reasonably general date and/or time parser that supports the common formats.
Dates are converted to YYYY-MM-DD format and times are converted to HH:MM:SS
format. These formats are suitable for MySQL.

Excel stores dates and times as a number representing the number of days since
1900-Jan-0, plus a fractional portion of a 24 hour day: ddddd.tttttt. These
floating point values are normally encountered when reading an Excel
spreadsheet. The date and/or time parsers handle this format too. Note that the
type of this parameter == type(0.0).

Call one of these:
  parse_date_time
  parse_date
  parse_time

They look for a date and/or time somewhere in the string.

Use one of these string constants to parse the string returned by one of the
parse functions into a datetime object using datetime.strptime().
  STD_DATE_FORMAT
  STD_TIME_FORMAT
  STD_DATE_TIME_FORMAT

Exercise the unit tests by running this from the ocmmand line.
"""


from datetime import datetime
from datetime import timedelta
import re


STD_DATE_FORMAT = '%Y-%m-%d'
STD_TIME_FORMAT = '%H:%M:%S'
STD_DATE_TIME_FORMAT = '%Y-%m-%d %H:%M:%S'

# List of (regex, strptime_format) tuples.
# Walks through the list, looking for a matching regex. If found, calls
# strptime to convert the string that matched into a datetime object.
regex_date_parse = [(re.compile(r'(\d\d)[a-zA-Z]{3}\d{4}'), '%d%b%Y'),            # DDMONYYYY
                    (re.compile(r'\d\d([-_./])[a-zA-Z]{3}\1\d{4}'), '%dx%bx%Y'),  # DDxMONxYYYY
                    (re.compile(r'\d\d([-_./])[a-zA-Z]{3}\1\d{2}'), '%dx%bx%y'),  # DDxMONx[YY]YY
                    (re.compile(r'\d{4}([-_./])\d?\d\1\d?\d'), '%Yx%mx%d'),       # YYYYx[M]Mx[D]D
                    (re.compile(r'\d?\d([-_./])\d?\d\1\d{4}'), '%mx%dx%Y'),       # [M]Mx[D]DxYYYY
                    (re.compile(r'\d?\d([-_./])\d?\d\1\d\d'), '%mx%dx%y'),        # [M]Mx[D]DxYY
                    (re.compile(r'\d?\d([-_./])\d?\d\d\d'),   '%mx%d%y'),         # [M]Mx[D]DYYYY
                    (re.compile(r'2\d{3}(0\d|1[012])([012]\d|3[01])'), '%Y%m%d'), # YYYYMMDD
                    ]
# where x is one of '-', '_', '.', or '/'.

regex_time_parse = [(re.compile(r'(\d?\d:\d\d:\d\d)'), '%H:%M:%S'),
                    (re.compile(r'(\d?\d:\d\d)'), '%H:%M'),
                    ]

def parse_date_time(dt):
    """
    Parse a string looking for a date and a time component, the order doesn't
    matter.  Convert an Excel float to date and time.
    Returns either None, or the date in YYYY-MM-DD HH:MM:SS format.
    """
    d = parse_date(dt)
    if d:
        t = parse_time(dt)
        if t:
            return d + ' ' + t
    return None


def parse_time(t):
    """
    Parse a string looking for a time component.  Convert an Excel float to
    time.
    Returns either None, or the time in HH:MM:SS format.
    """
    if type(t) == type(0.0):
        x = t % 1
        x = x * 24
        hour = int(x)
        x = x % 1
        x = x * 60
        minute = int(x)
        x = x % 1
        x = x * 60
        second = int(x)
        return '%02d:%02d:%02d' % (hour, minute, second)
    else:
        for (regex, date_parse) in regex_time_parse:
            mat = regex.search(t)
            if mat:
                date = datetime.strptime(mat.group(0), date_parse)
                break
        else:
            # Nothing matched.
            return None
    return date.strftime(STD_TIME_FORMAT)


def parse_date(d):
    """
    Parse a string looking for a date and a time component, the order doesn't
    matter.  Convert an Excel float to date.
    Returns either None, or the date in YYYY-MM-DD format.
    """
    if type(d) == type(0.0):
        base = datetime.strptime('1899/12/31', '%Y/%m/%d')        
        date = base + timedelta(d-1)
    else:
        for (regex, date_parse) in regex_date_parse:
            try:
                mat = regex.search(d)
                if mat:
                    # The \1 in the regex is 'x' in date_parse.
                    # Not all date_parse values have an x.
                    date_parse = date_parse.replace('x', mat.group(1))
                    date = datetime.strptime(mat.group(0), date_parse)
                    break
            except ValueError as e:
                # Got something like 9/31/1999 - bad day for month.
                pass
            except TypeError as e:
                # Couldn't parse. Ignore this one.
                pass
        else:
            # Nothing matched.
            return None
    # .strftime can't handle dates prior to 1900
    return date.isoformat()[:10]


if __name__ == '__main__':
    # Test cases for all 3 functions.
    n_tests = 0
    n_passed = 0
    for (string, expected) in [
        ('abc22may2011xyz', '2011-05-22'),
        ('21-Feb-11', '2011-02-21'),
        ('21_Feb_11', '2011-02-21'),
        ('21.Feb.11', '2011-02-21'),
        ('21/Feb/11', '2011-02-21'),
        ('2000-9-8', '2000-09-08'),
        ('2000_9_8', '2000-09-08'),
        ('2000.9.8', '2000-09-08'),
        ('2000/9/8', '2000-09-08'),
        ('8_7_2009', '2009-08-07'),
        ('8-7-2009', '2009-08-07'),
        ('8.7.2009', '2009-08-07'),
        ('8/7/2009', '2009-08-07'),
        ('8-7-12', '2012-08-07'),
        ('8_7_12', '2012-08-07'),
        ('8.7.12', '2012-08-07'),
        ('8/7/12', '2012-08-07'),
        ('20120328', '2012-03-28'),
        ('CPOE LAB audit 6.22.11 v2.xls', '2011-06-22'),
        ('7.7.11 CPOE Lab Audit.xls', '2011-07-07'),
        (37949.0, '2003-11-24'),
        ('1999/9/31', None),
        ('9999/31/10', None),
        ]:
        got = parse_date(string)
        n_tests += 1
        if got == expected:
            n_passed += 1
        else:
            print 'Failed test %d for parse_date, got %s, expected %s for string %s' % (n_tests, got, expected, string)
    for (string, expected) in [
        ('09:00', '09:00:00'),
        ('12:23:34', '12:23:34'),
        (0.763426, '18:19:20'),
        ]:
        got = parse_time(string)
        n_tests += 1
        if got == expected:
            n_passed += 1
        else:
            print 'Failed test %d for parse_time, got %s, expected %s for string %s' % (n_tests, got, expected, string)

    for (string, expected) in [
        ('2000_9_8 09:00', '2000-09-08 09:00:00'),
        ('02/18/2011 09:00', '2011-02-18 09:00:00'),
        ('02/18/2011 09:00:59', '2011-02-18 09:00:59'),
        (37949.763426, '2003-11-24 18:19:20'),
        ]:
        got = parse_date_time(string)
        n_tests += 1
        if got == expected:
            n_passed += 1
        else:
            print 'Failed test %d for parse_date_time, got %s, expected %s for string %s' % (n_tests, got, expected, string)

    print 'Out of %d date_time_parser tests, %d passed.' % (n_tests, n_passed)
