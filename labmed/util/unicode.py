#  @(#) $Id:  $  

### *********************************************************************
###
###   Copyright (c) 2012 University of Washington Laboratory Medicine
###   All Rights Reserved
###
###   The information contained herein is confidential to and the
###   property of University of Washington Laboratory Medicine and is
###   not to be disclosed to any third party without prior express
###   written permission of University of Washington Laboratory Medicine.
###   University of Washington Laboratory Medicine, as the
###   author and owner under 17 U.S.C. Sec. 201(b) of this work made
###   for hire, claims copyright in this material as an unpublished 
###   work under 17 U.S.C. Sec.s 102 and 104(a)   
###
### *********************************************************************


"""
Convert strings to 7-bit Unicode. If possible, convert to an HTML
character entity like &nbsp;.
"""

import htmlentitydefs


# Code points less than 160 not handled by htmlentitydefs.codepoint2name.
entity_map = {
    133:"&hellip;",
    145:"'",
    146:"'",
    147:'"',
    148:'"',
    149:'&bull;',                       # bullet
    150:'-',                            # en dash
    151:'-',                            # em dash
    153:'&trade;',                      # TM
    }


def to_char_entity(i):
    """
    Look up the ordinal value for a special character to see if we have a
    character entity for it. If there is none then replace return ' '.
    """
    if i in entity_map:
        return entity_map[i]
    if i in htmlentitydefs.codepoint2name:
        return '&' + htmlentitydefs.codepoint2name[i] + ';'
    # Use &# format :-(
    return '&#%d;' % i


def to_unicode(s):
    """Change characters we get a UnicodeEncodeError on to something else."""
    return ''.join([to_char_entity(ord(ch)) if ord(ch)>=128 else ch for ch in s])
