#  @(#) $Id:  $  

### *********************************************************************
###
###   Copyright (c) 2012 University of Washington Laboratory Medicine
###   All Rights Reserved
###
###   The information contained herein is confidential to and the
###   property of University of Washington Laboratory Medicine and is
###   not to be disclosed to any third party without prior express
###   written permission of University of Washington Laboratory Medicine.
###   University of Washington Laboratory Medicine, as the
###   author and owner under 17 U.S.C. Sec. 201(b) of this work made
###   for hire, claims copyright in this material as an unpublished 
###   work under 17 U.S.C. Sec.s 102 and 104(a)   
###
### *********************************************************************


"""
Defines classes and functions to make it easier to write code that processes a spreadsheet,
whether a CSV, .xls or .xlsx file, and store the data in a database.

If you are using argparse for command line parameters, use spreadsheet_parser
to specify the input_filename positional parameter and the optional --csv_sep
parameter.

There are two main classes you will use: Action and BasicRowHandler.

The Action class performs most of the work. It's constructor need an input file
for the spreadsheet, a RowHandler class, which we will discuss next, the
arguments to the RowHandler constructor, and a field separator for CSV files.
The Action constructor open the spreadsheet using the file's extension to
determine which type of reader is needed. If the imput file is a text file
(.txt or .csv) then it can be compressed using gzip (.gz). Such a file is
automatically uncompressed as it is read.

The main method you will call is action.loop(). This will read the each row of
the spreadsheet and call the RowHandler instance. You may want to set up a
try/except block to catch any exceptions.

A RowHandler class contains a handle method that is passws a list of cell
values for the current spreadsheet row. The types of these values are either
string, or they are None if the corresponding cell is empty.

There is a DefaultRowHandler class that assumes the column headers in the
spreadsheet correspond to column names in your database table. It simply reads
each row, uses the column names to store values into a database table object,
adds the object to the session, and commits the object to the database.

If you need a RowHandler to do more than that, use DefaultRowHandler as a
starting point. You may want to pass additional parameters to your constructor,
and perform special processing in the handle method. The BaseRowHandler class
defines a column_map to make it easy to retrieve data values by using the
column names. It is a dict where the key is the column name and the value is
the index into the spreadsheet row list.
"""


import argparse
import csv
import gzip
import openpyxl.reader.excel as xlsx
from openpyxl import __version__ as openpyxl_version
import os.path
import warnings
import xlrd


# Openpyxl changed index to be 1 based starting at 2.0.0.
openpyxl_first_index = 1 if openpyxl_version >= '2' else 0
openpyxl_oldest_working_version = '1.8.5'

class InputFileAction(argparse.Action):
    """
    Make sure the file exists and is readable.
    The argparse approach opens the file.
    """
    def __call__(self, parser, namespace, values, option_string=None):
        if os.path.isfile(values) and os.access(values, os.R_OK):
            setattr(namespace, self.dest, values)
        else:
            raise IOError('File %s must be a readable file.' % (values,))


def spreadsheet_parser(parser, what_file='spreadsheet'):
    """
    Define the input_filename command line parser.

    parser is an argparse parser.

    what_file is a string that says what the file contains.
    """
    parser.add_argument('--csv_sep', default=None,
                        help=("The character that separates columns."
                              " This only meaningful for CSV files."
                              r" Default: None - automatic determination of separator."))
    parser.add_argument('input_filename', action=InputFileAction,
                        help=('This is the file that contains %s data.' % (what_file,)))


#------------------------------------------------------------------------------
# Spreadsheet reader classes for all 3 types of spreadsheets.
#------------------------------------------------------------------------------
class CSVSpreadsheet(object):
    """
    Class that handles spreadsheets in CSV format.
    """
    def __init__(self, filename, csv_sep=None):
        if csv_sep:
            csv_sep = eval('"' + csv_sep + '"')
            if len(csv_sep) != 1:
                raise Exception('Length of csv_sep must be 1, was %d.' % (len(csv_sep),))
        file = (gzip.GzipFile if filename.endswith('.gz') else open)(filename, 'r')
        if not csv_sep:
            # No csv_sep specified. Try to determine what it is.
            dialect = csv.Sniffer().sniff(file.read(min(1024, os.path.getsize(filename))))
            file.seek(0)
            csv_sep = dialect.delimiter
        self.csv_reader = csv.reader(file, delimiter=csv_sep)
        self.more_data = True

    def reader(self):
        """
        Generator function to retrieve data values, one row at a time.
        """
        for row in self.csv_reader:
            yield row

    def header_row(self):
        """
        Just get the column headers. Call multiple times for multi-line headers.
        """
        return self.csv_reader.next()


class ExcelSpreadsheet(object):
    """
    Class that handles excel 1997-2003 spreadsheets.
    """
    def __init__(self, filename):
        book = xlrd.open_workbook(filename)
        self.sheet = book.sheet_by_index(0)
        self.n_rows = self.sheet.nrows
        self.header_row_index = 0

    def reader(self):
        for this_row in xrange(1, self.n_rows):
            types = self.sheet.row_types(this_row)
            values = self.sheet.row_values(this_row)
            yield [None if types[i] == xlrd.XL_CELL_EMPTY else self.cnv(values[i]) for i in xrange(len(values))]

    def cnv(self, value):
        if type(value) == type(1.0) and value % 1 == 0:
            # Remove trailing '.0'.
            return '%d' % (value,)
        return str(value)

    def header_row(self):
        types = self.sheet.row_types(self.header_row_index)
        values = self.sheet.row_values(self.header_row_index)
        self.header_row_index += 1
        return [None if types[i] == xlrd.XL_CELL_EMPTY else str(values[i]) for i in xrange(len(values))]

class ExcelxSpreadsheet(object):
    """
    Class that handles excel xlsx spreadsheets.
    """
    def __init__(self, filename):
        # Earlier versions are broken.
        if [int(e) for e in openpyxl_version.split('.')] < [int(e) for e in openpyxl_oldest_working_version.split('.')]:
            raise Exception('Version %s of openpyxl is contains bugs preventing its use. Install new version >= %s'
                            % (openpyxl_version, openpyxl_oldest_working_version))
        book = xlsx.load_workbook(filename)
        self.sheet = book.get_active_sheet()
        self.n_rows = self.sheet.get_highest_row()
        self.n_cols = self.sheet.get_highest_column()
        self.header_row_index = openpyxl_first_index

    def reader(self):
        for this_row in xrange(self.header_row_index, self.n_rows+openpyxl_first_index):
            yield [None if self.sheet.cell(row=this_row, column=i).value is None else str(self.sheet.cell(row=this_row, column=i).value) for i in xrange(openpyxl_first_index,self.n_cols+openpyxl_first_index)]

    def header_row(self):
        ret = [None if self.sheet.cell(row=self.header_row_index, column=i).value is None else str(self.sheet.cell(row=self.header_row_index, column=i).value) for i in xrange(openpyxl_first_index,self.n_cols+openpyxl_first_index)]
        self.header_row_index += 1
        return ret


#------------------------------------------------------------------------------
# Action class
#------------------------------------------------------------------------------
class Action(object):
    """
    This class implements the main read loop. It is always the same,
    so there is no point duplicating.
    """
    def __init__(self, input_filename, RowHandlerClass, row_handler_args, csv_sep = None,
                 column_name_reader = None):
        """
        Create an Action instance to read the spreadsheet.
        input_filename - the name of the input file.
        RowHandlerClass - a class object that implements the processing action
          needed for each row. Can use DefaultRowHandler. If you define your own,
          be sure to use BaseRowHandler as your base class.
        row_handler_args - dict of the arguments for the RowHandlerClass
          constructor. Note that constructor parameter 'column_names' is passed
          in automatically, since it is defined in this constructor.
        csv_sep - optional string constant for CSV files.
        column_name_reader - optional alternate way to read column names.
          Useful when the column names span multiple spreadsheet rows. Its only
          argument is the spreadsheet object.
        """
        self.count = 0
        self.sheet = self.open_sheet(input_filename, csv_sep)
        self.column_names = (column_name_reader(self.sheet)
                             if column_name_reader
                             else self.mangle_column_headers(self.sheet.header_row()))
        self.row_handler = RowHandlerClass(column_names=self.column_names, **row_handler_args)
        self.empty_row = [None] * len(self.column_names)

    def loop(self):
        """
        The main loop for an action function.
        """
        for row in self.sheet.reader():
            if row == self.empty_row:
                # Skip empty rows
                continue
            self.row_handler.handle(row)
            self.count += 1
    
    #--------------------------------------------------------------------------
    # Support methods. No need to call these directly.
    #--------------------------------------------------------------------------
    def open_sheet(self, input_filename, csv_sep=None):
        # the [1] gets the extension and the [1:] removes the '.' from the extension.
        file_type = os.path.splitext(input_filename[:-3] if input_filename.endswith('.gz')
                                     else input_filename)[1][1:]
        # The following code will cause this warning to ignored, instead of printing to stderr.
        warnings.filterwarnings(action="ignore", message="BaseException.message has been deprecated")
        warnings.filterwarnings(action="error", message="Data truncated for column .*")

        if file_type in self.spreadsheet_type_mapper:
            (SpreadsheetClass, open_args) = self.spreadsheet_type_mapper[file_type]
            return SpreadsheetClass(input_filename, **eval(open_args))
        else:
            raise Exception('Unknown file type: %s. Expected one of these: %s' %(file_type, sorted(self.spreadsheet_type_mapper.keys())))


    def mangle_column_headers(self, headers):
        """
        Convert column headers in the spreadsheet into column names.
        Removes trash characters and appends an underscore between each
        word. If a header was 'CollDate_Time' it would be converted to
        'coll_date_time'.
        """
        column_names = []
        column_index = 0
        for header in headers:
            column_index += 1
            try:
                for (old, new) in self.replacements:
                    header = header.replace(old,new)
                # Remove leading and trailing underscores.
                while len(header) and header[0] == '_':
                    header = header[1:]
                while len(header) and header[-1] == '_':
                    header = header[:-1]
                header = '_'.join(self.extract_words(header))
                if header in self.reserved_replacements:
                    header = self.reserved_replacements[header]
                column_names.append(header)
            except AttributeError:
                if headers[column_index-1:] == [None] * (len(headers) - column_index + 1):
                    # The rest of the headers are empty, ignore them.
                    break
                print 'Unable to handle column %d of %d: %s' % (column_index, len(headers), header)
                raise
        return column_names


    def extract_words(self, header):
        """
        Works hard to extract words from a header. Returns a list of the words.
        Works for cases like:
            PriceCode -> price_code
            CPT_Codes -> cpt_codes
            Codes_CPT_Number -> codes_cpt_number
        """
        if header.isupper():
            # This header is all upper case - only 1 word.
            return [header.lower()]
        if header.isalpha() and header.islower():
            # All done, only one word.
            return [header]
        words = []
        word = ''
        r = list(header)
        r.reverse()
        had_upper = False
        for ch in r:
            if ch == '_':
                if word:
                    words.append(word.lower())
                    word = ''
            else:
                word = ch + word
                if ch.isupper():
                    if not had_upper and len(word) > 1:
                        # Found something like 'Code'.
                        # Found a word like 'Code'.
                        words.append(word.lower())
                        word = ''
                    else:
                        # Either had_upper or len(word) == 1
                        had_upper = True
                else:
                    had_upper = False
        if word:
            words.append(word.lower())
        return reversed(words)


    # Dictionary for each file_type. Contents are:
    #   The class object to handle the spreadsheet,
    #   String to evaluate to pass extra arguments to the spreadsheet's open
    #     method. Assumes that 'csv_sep' is defined in the context.
    spreadsheet_type_mapper = {
        'xls':  (ExcelSpreadsheet,  "{}"),
        'csv':  (CSVSpreadsheet,    "{'csv_sep': csv_sep}"),
        'txt':  (CSVSpreadsheet,    "{'csv_sep': csv_sep}"),
        'dat':  (CSVSpreadsheet,    "{'csv_sep': csv_sep}"), # Test tally files.
        'lis':  (CSVSpreadsheet,    "{'csv_sep': csv_sep}"), # LIS feed file: bcDump-LABA.lis
        'xlsx': (ExcelxSpreadsheet, "{}"),
        }


    # Characters in a column header that need to be replaced to become a normal SQL name.
    replacements = [('#', '_number_'),
                    ('*', ''),
                    ('.', ''),
                    ('(', ''),
                    (')', ''),
                    ('\r', ''),
                    ('?', ''),          # Unfortunately this doesn't always indicate a boolean field.
                    ('\n', '_'),
                    (',', '_'),
                    ('-', '_'),
                    (' ', '_'),
                    ('/', '_'),
                    ('\\', '_'),
                    ('"', '_'),
                    ("'", '_'),
                    ('__', '_')]


    # Reserved words and their replacements.
    # Nothing yet.
    reserved_replacements = {

        }



#------------------------------------------------------------------------------
# RowHandler classes.
#------------------------------------------------------------------------------

class BaseRowHandler(object):
    """
    Base row handler does nothing.
    """
    def __init__(self, session, DbObjectClass, column_names, only_print):
        self.session = session
        self.DbObjectClass = DbObjectClass
        self.column_names = column_names
        self.only_print = only_print
        # Use this to get row values by name.
        self.column_map = dict([(self.column_names[i], i) for i in range(len(self.column_names))])


    def handle(self, row):
        pass


class DefaultRowHandler(BaseRowHandler):
    """
    Simple row handler that loads data into the database using the spreadsheet
    column names as database column names.
    """
    def __init__(self, session, DbObjectClass, column_names, only_print):
        BaseRowHandler.__init__(self, session, DbObjectClass, column_names, only_print)
        
    def handle(self, row):
        o = self.DbObjectClass()
        for i in xrange(len(self.column_names)):
            o.set(self.column_names[i], row[i])
        if self.only_print:
            print row
        else:
            self.session.add(o)
            self.session.commit()
