"""
This module contains a function that tries to extract a version number from a
file, normally one that comes from the LIS. This is used for the Genetics
mrRESgen and OLTG bcDump files.
"""


def get_version(infile):
    """infile - an open input file.

Returns (revision_timestamp, report_timestamp, True) if the first line of
infile is of the form:

    #<report-timestamp (yyyy-mm-dd HH:MM:SS)>|<revision-timestamp (yyyy-mm-dd HH:MM)>

or (None, None, False) if the first line is not of that form.

The boolean in the third element of the triple indicates whether or not the
version number was found.

For example:

    #2014-03-25 07:28:35|2014-03-17 12:30

would return ('2014-03-17 12:30', '2014-03-25 07:28:35', True)

If there is a version line, this function has the side effect of advancing to
the second line of the file, so that the next line is expected to be the
header.
    """
    line = next(infile)
    if line.startswith('#'):
        report_ts, format_ts = line[1:].strip().split('|', 1)
        val = (format_ts, report_ts, True)
    else:
        val = (None, None, False)
        infile.seek(0)
    return val
