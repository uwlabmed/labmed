#  @(#) $Id:  $  

### *********************************************************************
###
###   Copyright (c) 2012 University of Washington Laboratory Medicine
###   All Rights Reserved
###
###   The information contained herein is confidential to and the
###   property of University of Washington Laboratory Medicine and is
###   not to be disclosed to any third party without prior express
###   written permission of University of Washington Laboratory Medicine.
###   University of Washington Laboratory Medicine, as the
###   author and owner under 17 U.S.C. Sec. 201(b) of this work made
###   for hire, claims copyright in this material as an unpublished 
###   work under 17 U.S.C. Sec.s 102 and 104(a)   
###
### *********************************************************************


"""
This program is used in conjunction with js/FilterSelect.js to filter an HTML SELECT list.
Use this when there is a long list of names. The user enters text into a small
box next to the SELECT list and the menu gets filtered based on that list.
Actually uses regular expressions to allow for more general filtering.

Usage:
Generate Javascript code to fill in 2 arrays, one with the IDs of the objects,
the other with the corresponding names.

Call the filter_select_create function (here) to create a python FilterSelect
object. After the 2 arrays are initialized, call the generate_javascript()
method to create JS code to create a JS FilterSelect object, passing the
parameters, using parameters to allow for more than one FilterSelect SELECT on
a web page.

Call the generate_input_attributes() method to construct the small <input>
element. Look at its documentation for usage examples.
"""


from datetime import datetime
import os


class FilterSelect(object):
    def __init__(self, select_element_name, text_items, value_items):
        self.select_element_name = select_element_name
        self.text_items = text_items
        self.value_items = value_items
        self.ignore_case = False
        self.change_handler = None
        self.help_text = None

    def setIgnoreCase(self):
        self.ignore_case = True


    def setChangeHandler(self, change_handler):
        self.change_handler = change_handler


    def setHelpText(self, help_text):
        self.help_text = help_text


    def generate_javascript(self):
        """
        Generate the JS code to create a FilterSelect object.
        Put this in a <script> element.
        """
        select_element_pieces = self.select_element_name.split('.')
        # Need unique JavaScript names. Use the SELECT element name for that
        base_name = select_element_pieces[-1]
        # This holds the name of the FilterSelect JavaScript object.
        filter_select_object = base_name + "FilterSelect"
        # This is the NAME for the text element.
        text_element_name = base_name + "FilterSelectText";
        # This is the ID attribute for the text element.
        text_id = base_name + "FilterSelectTextID"
        return """
var %s = new FilterSelect(%s, %s, %s);%s%s
""" % (filter_select_object, self.select_element_name, self.text_items, self.value_items,
       ('\n' + filter_select_object + '.setIgnoreCase();') if self.ignore_case else '',
       ('\n' + filter_select_object + '.setChangeHandler(' + self.change_handler +
        ');') if self.change_handler else '')


    def generate_input_attributes(self):
        """
        Use this in an <input> element. Use something like the following to
        generate HTML code for the small input element:

          '<input ' + ' '.join(['%s="%s"' % (k,d[k]) for k in sorted(d.keys())]) + '>'
        
        d is the return value. If using in the <TAL> markup language, pass the
        return value to a tal:attributes attribute for an <input> element.
        Something like this:

          <input tal:define="atts python:fs.generate_input_attributes()"
              tal:attributes="type python:atts['type']; name python:atts['name']; id python:atts['id'];
              size python:atts['size'];
              onchange python:atts['onchange'];
              onkeyup python:atts['onkeyup'];
              onmouseover python:atts['onmouseover'];
              onmouseout python:atts['onmouseout'];
              onmouseup python:atts['onmouseup']">

        where fs is a Python FilterSelect object from filter_select_create().

        Returns:
          Dictionary of attributes with the following keys:
            type, name, id, size, onchange, onkeyup, onmouseover, onmouseout, onmouseup, title
        """
        select_element_pieces = self.select_element_name.split('.')
        # Need unique JavaScript names. Use the SELECT element name for that
        base_name = select_element_pieces[-1]
        # This holds the name of the FilterSelect JavaScript object.
        filter_select_object = base_name + "FilterSelect"
        # This is the NAME for the text element.
        text_element_name = base_name + "FilterSelectText";
        # This is the ID attribute for the text element.
        text_id = base_name + "FilterSelectTextID"
        return {'type':"text", 'name':text_element_name, 'id':text_id, 'size':"5",
                'onchange':("performSelectFiltering(%s, this.value)" % (filter_select_object,)),
                'onkeyup':("return filterSelectHandler(event, '%s')" % (filter_select_object,)),
                'onmouseover':("window.defaultStatus='%s'" % (self.help_text if self.help_text else 'null',)),
                'onmouseout':"window.defaultStatus=''",
                'onmouseup':("return filterSelectHandler(event, '%s')" % (filter_select_object,)),
                'title': self.help_text if self.help_text else ''
}


def filter_select_create(select_element_name, text_items, value_items, ignore_case=False, help_text=None):
    """
    Create a FilterSelect object so we can generate the code that is needed.

    Params:
      select_element_name - the full JavaScript element name for the SELECT
      list being filtered.

      text_items JavaScript variable name that contains the text items that the
      user sees in the SELECT list.

      value_items JavaScript variable name that contains the value items that
      are associated with the text items in the SELECT list.

      ignore_case - True if case is to be ignore, False otherwise. Default is False.

      help_text - help text if the user moves the mouse into the small text
      box. Default is None.
    """
    # Listed these files:
    # ['extensions', 'config.ini', 'schema.py', 'roundup.cgi', '.htaccess',
    #  'detectors', 'html', 'db', 'initial_data.py', 'FilterSelect.js']
    # Should be able to write a loader for JS code.
    fs = FilterSelect(select_element_name, text_items, value_items)
    if ignore_case:
        fs.setIgnoreCase()
    if help_text:
        fs.setHelpText(help_text)
    return fs


if __name__ == '__main__':
    fs = filter_select_create('document.main.sel_name', 'foo_names', 'foo_values', True, 'Random help text.')
    print fs.generate_javascript()
    print fs.generate_input_attributes()
